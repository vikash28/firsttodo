from __future__ import print_function

import io
import json
from openpyxl import Workbook, load_workbook


class ExcelEndOfData(Exception):
    '''Raised to signal that there are no more rows to be processed'''
    pass


class WorksheetDefinition(object):
    '''
    A object focusing on converting working rows into dictionary.

    WIP: This is not completed code and not being used.
    '''
    _headers = None
    _header_row = None
    _cur_row = 0

    def __init__(self, header_row=None, headers=None):
        '''Set the header row or headers.  If both passed, only header_row will be used'''
        if header_row:
            self._header_row = header_row
        elif headers:
            self._headers = headers

    def set_row(self, current_row=None):
        '''The the current row.  If current_row is not passed, increament by one'''
        if current_row is None:
            self._cur_row += 1
        else:
            self._cur_row = current_row

    def rtrim_none(self, in_list):
        '''Remove None from the end of the list'''
        while in_list and in_list[-1] is None:
            in_list.pop()

    def ltrim_none(self, in_list):
        '''Remove None from the start of the list'''
        while in_list and in_list[0] is None:
            in_list.pop(0)

    def trim_none(self, in_list):
        '''Remove None from beginning and end of the list'''
        self.rtrim_none(in_list)
        self.ltrim_none(in_list)

    def format_row(self, in_list):
        '''Format the row'''
        pass
        

    def validate_row(self, in_list):
        '''Override to validate the row'''
        pass



class ExcelParser(object):
    '''
    Object used to actually parse the content of the Excel.  Sequence called:

    1. sheet_processor:    Process sheet if needed.
    2. row_pre_processor:  Format the raw rows from the spreadsheet
    3. row_processor:      Create list or dict depending if header_row is set
    4. row_post_processor: Final format of the resulting row
    '''
    headers = None
    _header_row = None
    _row_count = 0

    def __init__(self, header_row=None, headers=None):
        if header_row:
            self._header_row = header_row
        if headers:
            self.headers = headers

    def incr_row(self, increment=None):
        if increment:
            self._row_count = self._row_count + increment
        else:
            self._row_count += 1

    def row_count(self):
        return self._row_count

    def rtrim_none(self, in_list):
        '''Remove None from the end of the list'''
        while in_list and in_list[-1] is None:
            in_list.pop()

    def trim_none(self, in_list):
        '''Remove None from beginning and end of the list'''
        self.rtrim_none(in_list)
        while in_list and in_list[0] is None:
            in_list.pop(0)

    def row_post_processor(self, row_result):
        return row_result

    def row_pre_processor(self, in_list):
        '''Format a row'''
        if in_list:
            self.rtrim_none(in_list)
            return in_list

    def row_processor(self, row):
        '''Process a row.  Called by ExcelProcessor'''
        if row:
            result = self.row_pre_processor([x.value for x in row])
            if result:
                if self.headers is None:
                    return self.row_post_processor(result)
                else:
                    if self._header_row > self.row_count():
                        # Skip rows before header row
                        return
                    elif self._header_row == self.row_count():
                        # Capture the header only if needed
                        # ToDo: print out warning if captured header is different
                        if self.headers is None:
                            self.headers = [str(x) for x in result]
                        return
                    else:
                        # Return result
                        return self.row_post_processor(dict(zip(self.headers, result)))

    def sheet_processor(self, sheet):
        '''Process a sheet.  Called by ExcelProcessor'''
        return sheet

    def end_row_processing(self):
        raise ExcelEndOfData()


class ExcelProcessor(object):
    '''Main Program'''
    _loggername = "proc"
    def __init__(self, fname, parser=None):
        if parser:
            self.parser = parser
        else:
            self.parser = ExcelParser()

        self._wb = load_workbook(fname)
        self._result = []

    def row_processor(self, row):
        result = self.parser.row_processor(row)
        if result:
            self._result.append(result)

    def sheet_processor(self, sheet):
        sht = self.parser.sheet_processor(sheet)
        if sht:
            print("- processing sheet %s" % sht.title)
            for row in sht:
                self.parser.incr_row()
                try:
                    self.row_processor(row)
                except ExcelEndOfData:
                    break
                except StandardError as e:
                    raise RuntimeError("%s\n[%d] Failed on row %s" % (e, self.parser.row_count(), str([x.value for x in row])))

    def save(self, outfile, indent=4):
        '''Output the UTF8 JSON'''
        with io.open(outfile, "w+", encoding='utf8') as fh:
            fh.write(json.dumps(self._result, indent=indent, ensure_ascii=False))
        # Clear result
        self._result = []

    @classmethod
    def load(self, jsonfile):
        with io.open(jsonfile, "r", encoding='utf8') as fh:
            result = json.load(fh)
        return result

    def process(self, out_fname=None):
        '''Read the result'''
        print("# START Processing Excel Template")
        for sheet in self._wb:
            self.sheet_processor(sheet)

        if out_fname:
            print("- END Parse completed.  Saving to %s" % out_fname)
            self.save(out_fname)
        else:
            print("- END Parse completed.")

    def get_result(self):
        return self._result


class ExcelWriter(object):
    def __init__(self, fname):
        self._fname = fname
        self._wb = Workbook(write_only=True)

    def row_format(self, in_dict):
        pass

    def write_sheet(self, name, headers, in_list):
        sht = self._wb.create_sheet(title=name)
        # create header
        sht.append(headers)
        # create content
        for entry in in_list:
            self.row_format(entry)
            data = [entry.get(x) for x in headers]
            sht.append(data)

    def save(self):
        self._wb.save(self._fname)

