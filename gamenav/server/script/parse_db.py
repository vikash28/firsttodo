#!/usr/bin/env python

from __future__ import print_function

import os
import numbers

from lib.excel import ExcelWriter, ExcelParser, ExcelProcessor
from lib.db import SimpleDatabase

INPUT_EXCEL_FILE = "../resource/db.xlsx"
OUTPUT_JSON_FILE = "./output/db.json"
OUTPUT_SQLITE_FILE = "./output/db.sqlite"
OUTPUT_EXCEL_FILE = "./output/result.xlsx"

DDL_TABLE_NAME = "gamenav"
DDL = '''
create table %s (
    family_id int,
    age int,
    age_group text,
    primary_residence int,
    secondary_residence int,
    vehicle int,
    children int,
    degree text,
    profession text,
    total_asset numeric,
    total_invest numeric,
    total_income numeric,
    primary key (family_id)
)
''' % DDL_TABLE_NAME

SQL_RESULT = '''
select
    family_id,
    age,
    age_group,
    primary_residence,
    secondary_residence,
    vehicle,
    children,
    degree,
    profession,
    avg(total_asset) total_asset,
    avg(total_invest) total_invest,
    avg(total_income) total_income
from
    gamenav
group by
    age,
    age_group,
    primary_residence,
    secondary_residence,
    vehicle,
    children,
    degree,
    profession
'''





class DBParser(ExcelParser):
    headers = [
        'family_id', 'age', 'age_group', 'primary_residence', 'secondary_residence',
        'vehicle', 'children', 'degree', 'profession',
        'total_asset', 'total_invest', 'total_income'
    ]
    _string_headers = set(['age_group', 'degree', 'profession'])

    def row_pre_processor(self, in_list):
        if in_list:
            self.trim_none(in_list)
            return in_list

    def _check_row_data(self, in_dict):
        for key in in_dict.keys():
            data = in_dict[key]

            if data is None:
                del in_dict[key]
            elif key == 'family_id' and data == 'END':
                self.end_row_processing()
            elif key in self._string_headers:
                if not isinstance(data, basestring):
                    raise ValueError("Expected '%s' to contain string but got '%s'" % (key, data))
            else:
                if not isinstance(data, numbers.Number):
                    raise ValueError("Expected '%s' to contain number but got '%s'" % (key, data))

                if key.startswith("total"):
                    in_dict[key] = float(data)

    def row_post_processor(self, row_result):
        '''validate the result'''
        if row_result:
            if not isinstance(row_result, dict):
                raise ValueError("Expected row to bes a dictionary but got: %s" % row_result)
            self._check_row_data(row_result)
            return row_result

    def sheet_processor(self, sheet):
        if sheet.title == "DATABASE":
            return sheet


class GameNavExcelWriter(ExcelWriter):
    ROUND_TOTAL_MAP = {
        "total_asset": 50000.0,
        "total_invest": 1000.0,
        "total_income": 5000.0
    }

    def round_to_closest(self, val, round_to):
        return round(float(val)/float(round_to)) * round_to

    def row_format(self, in_dict):
        for key, round_to in self.ROUND_TOTAL_MAP.iteritems():
            in_dict[key] = self.round_to_closest(in_dict[key], round_to)
            print("### rounded %s to: %s" % (key, in_dict[key]))


if __name__ == "__main__":
    if not os.path.exists(OUTPUT_JSON_FILE):
        proc = ExcelProcessor(INPUT_EXCEL_FILE, DBParser(header_row=7))
        proc.process(OUTPUT_JSON_FILE)

    db = SimpleDatabase(OUTPUT_SQLITE_FILE, init_script=DDL)
    if not db.db_exists():
        # Initialize database
        first_row = True
        insert_sql = None
        fields = None
        for entry in ExcelProcessor.load(OUTPUT_JSON_FILE):
            if first_row:
                insert_sql = db.sql_insert(DDL_TABLE_NAME, DBParser.headers)
                first_row = False
            else:
                params = db.sql_params(entry, DBParser.headers)
                db.execute(insert_sql, params)

    out_fields = [
        'age', 'age_group', 'primary_residence', 'secondary_residence',
        'vehicle', 'children', 'degree', 'profession',
        'total_asset', 'total_invest', 'total_income'
    ]

    wrt = GameNavExcelWriter(OUTPUT_EXCEL_FILE)
    wrt.write_sheet("result", out_fields, db.get(SQL_RESULT))
    wrt.save()
