/*
 * AgeSex
 *
 * The reducer takes care of our data. Using actions, we can change our
 * application state.
 * To add a new action, add it to the switch statement in the reducer function
 *
 * Example:
 * case YOUR_ACTION_CONSTANT:
 *   return state.set('yourStateVariable', true);
 */
import { fromJS } from 'immutable';
import {
  CHANGE_AGESEX,
} from './constants';

// The initial state of the App
const initialState = fromJS({
  agesex: 'X',
});

function ageSexReducer(state = initialState, action) {
  console.log(action.name);
  switch (action.type) {
    case CHANGE_AGESEX:
      return state
        .set('agesex', action.name);
    default:
      return state;
  }
}

export default ageSexReducer;
