/**
 * AgeSex selectors
 */

import { createSelector } from 'reselect';

const selectAge = (state) => state.get('age');

const makeSelectAge = () => createSelector(
  selectAge,
  (ageState) => ageState.get('agesex')
);

export {
  selectAge,
  makeSelectAge,
};
