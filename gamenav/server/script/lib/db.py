from __future__ import print_function

import os
import sqlite3
import tempfile

class SimpleDatabase(object):
    '''A local sqlite3 database for storage and aggregation'''
    _dbfile = None

    @classmethod
    def dict_factory(cls, cursor, row):
        d = {}
        for idx, col in enumerate(cursor.description):
            d[col[0]] = row[idx]
        return d

    def __init__(self, fname=None, init_script=None):
        db_exists = False
        if fname is None:
            self._dbfile = tempfile.NamedTemporaryFile(suffix='.sqlite3')
            fname = self._dbfile.name
        elif os.path.exists(fname):
            db_exists = True
        self.db = sqlite3.connect(fname)
        self.db.row_factory = self.dict_factory

        if not db_exists and init_script:
            print("- Running db init script...")
            self.execute(init_script)

        self._db_exists = db_exists

    def db_exists(self):
        return self._db_exists

    def close(self):
        self.db.close()
        if self._dbfile:
            self._dbfile.close()

    def execute(self, sql, *params):
        cur = self.db.cursor()
        cur.execute(sql, *params)
        self.db.commit()

    def get(self, sql, *params):
        cur = self.db.cursor()
        cur.execute(sql, params)
        for row in cur:
            yield row

    def get_first(self, sql, *params):
        for entry in self.get(sql, *params):
            return entry

    def sql_insert(self, table_name, fields):
        '''Generate an insert statement given the fields'''
        return "INSERT INTO %s (%s) VALUES (%s);" % (table_name, ','.join(fields), ','.join(['?'] * len(fields)))

    def sql_params(self, in_dict, fields):
        '''Return a params list, with values from input dictionary with key provided by fields, in the sequence of fields'''
        params = []
        for field in fields:
            params.append(in_dict.get(field))
        return params
