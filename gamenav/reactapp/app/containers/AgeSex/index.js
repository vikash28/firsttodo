/*
 * AgeSex
 *
 * This will allow a user to select one of 12 images to indicate their sex and age range
 * Located at the '/agesex' route
 *
 */

import React from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import { Page, Row, Column } from 'hedron';
import PersonImage from './person.png';
import { makeSelectAge } from './selectors';
import { changeAgeSex } from './actions';
//


function Square(props) {
  return (
    <button className="square" onClick={props.onClick}>
      <img src={PersonImage}  id={props.value} />
      <div id={props.value}>
        {props.value}
      </div>
    </button>
  );
}

export class AgeSex extends React.PureComponent {
  render() {
    return (
      <div>
        <Row divisions={4}>
          <Column xs={2} sm={2} md={1} lg={1}>
            <Column xs={4} sm={2} md={1} lg={1}>
              <Square value="A" onClick={this.props.onSelectSquare}/>
            </Column>
            <Column xs={4} sm={2} md={1} lg={1}>
              <Square value="B" onClick={this.props.onSelectSquare}/>
            </Column>
            <Column xs={4} sm={2} md={1} lg={1}>
              <Square value="C" onClick={this.props.onSelectSquare}/>
            </Column>
          </Column>
          <Column xs={2} sm={2} md={1} lg={1}>
            <Column xs={4} sm={2} md={1} lg={1}>
              <Square value="D" onClick={this.props.onSelectSquare}/>
            </Column>
            <Column xs={4} sm={2} md={1} lg={1}>
              <Square value="E" onClick={this.props.onSelectSquare}/>
            </Column>
            <Column xs={4} sm={2} md={1} lg={1}>
              <Square value="F" onClick={this.props.onSelectSquare}/>
            </Column>
          </Column>
          <Column xs={2} sm={2} md={1} lg={1}>
            <Column xs={4} sm={2} md={1} lg={1}>
              <Square value="1" onClick={this.props.onSelectSquare}/>
            </Column>
            <Column xs={4} sm={2} md={1} lg={1}>
              <Square value="2" onClick={this.props.onSelectSquare}/>
            </Column>
            <Column xs={4} sm={2} md={1} lg={1}>
              <Square value="3" onClick={this.props.onSelectSquare}/>
            </Column>
          </Column>
          <Column xs={2} sm={2} md={1} lg={1}>
            <Column xs={4} sm={2} md={1} lg={1}>
              <Square value="4" onClick={this.props.onSelectSquare}/>
            </Column>
            <Column xs={4} sm={2} md={1} lg={1}>
              <Square value="5" onClick={this.props.onSelectSquare}/>
            </Column>
            <Column xs={4} sm={2} md={1} lg={1}>
              <Square value="6" onClick={this.props.onSelectSquare}/>
            </Column>
          </Column>
        </Row>
        Selected: {this.props.age}

      </div>
    );
  }
}

AgeSex.propTypes = {
  onSelectSquare: React.PropTypes.func,
  age: React.PropTypes.string
};

export function mapDispatchToProps(dispatch) {
  return {
    onSelectSquare: (evt) => {
      dispatch(changeAgeSex(evt.target.id))
    }
  };
}

const mapStateToProps = createStructuredSelector({
  age: makeSelectAge()
});

// Wrap the component to inject dispatch and state into it
export default connect(mapStateToProps, mapDispatchToProps)(AgeSex);
