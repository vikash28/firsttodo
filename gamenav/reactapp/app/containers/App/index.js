/**
 *
 * App.react.js
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import { slide as Menu } from 'react-burger-menu'
import { menuStyle, aStyle, mainDivStyle } from './styles';

export default class App extends React.PureComponent {
  static propTypes = {
    children: React.PropTypes.node,
  };

  render() {
    return(
      <div id="outer-container">
        <Menu pageWrapId={ "page-wrap" } outerContainerId={ "outer-container" } styles={ menuStyle }>
          <a id="home" className="menu-item" href="/" style={ aStyle }>Home</a>
          <a id="agesex" className="menu-item" href="/agesex" style={ aStyle }>Age & Sex</a>
          <a id="carsowned" className="menu-item" href="/carsowned" style={ aStyle }>Cars Owned</a>
        </Menu>
        <main id="page-wrap">
          <div style={ mainDivStyle }>
            {React.Children.toArray(this.props.children)}
          </div>
        </main>
      </div>

    );
  }
}
