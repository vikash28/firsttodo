/*
 * CarsOwned
 *
 */

import React from 'react';

export default class CarsOwned extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    const divStyle = { textAlign: 'center' };
    const message = "This will be the cars owned page";
    return (
      <div style={ divStyle }>
        { message }
      </div>
    );
  }
}
