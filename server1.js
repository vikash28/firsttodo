var express= require('express');
var app =express();

var bodyParser= require('body-parser');
var mongoose = require('mongoose');  
var morgan = require('morgan');             // log requests to the console (express4)
var methodOverride = require('method-override'); // simulate DELETE and PUT (express4)

mongoose.connect('mongodb://127.0.0.1:27017/test');
app.use(express.static(__dirname + '/public'));
app.use(morgan('dev'));                                         // log every request to the console

app.use(bodyParser.urlencoded({'extended':'true'}));            // parse application/x-www-form-urlencoded

app.use(bodyParser.json())
app.use(bodyParser.json({ type: 'application/vnd.api+json' })); // parse application/vnd.api+json as json

app.use(methodOverride());



var Todo = mongoose.model('Todo',
{
        text : String
});
 
app.get('/api/todos',  function(req, res) {
	
	Todo.find(function(err,data)
	{
		if(err)	{ res.send(err) };

		console.log(data);
		res.json(data);
	})
})

app.post('/api/todos',  function(req, res) { 
	Todo.create({
		text:req.body.text,
		 done : false
	},function(err,data){
		if(err) { res.send(err); }	

		Todo.find(function(err, todos) {
                if (err)
                   {  res.send(err) }
                res.json(todos);
            });
	})
})

// delete a todo
app.delete('/api/todos/:todo_id', function(req, res) {
        Todo.remove({
            _id : req.params.todo_id
        }, function(err, todo) {
            if (err)
                res.send(err);

            // get and return all the todos after you create another
            Todo.find(function(err, todos) {
                if (err)
                    res.send(err)
                res.json(todos);
            });
        });
});

app.get('*', function(req, res) {
        res.sendfile('./public/index.html'); // load the single view file (angular will handle the page changes on the front-end)
});

app.listen('8080');
console.log("App listening on port 8080");